# cashc-web

This is a workaround that allows `cashc` to work in Web environemnts.

The underlying parser library used by CashScript (`antlr4ts`) has circular dependencies and will not bundle correctly with some tools (e.g. `rollup`).

This causes runtime errors: https://github.com/tunnelvisionlabs/antlr4ts/issues/466

Webpack does not appear to have this issue, so it is used here to bundle `cashc` AND `antlr4ts` into a single package (with polyfills).

## Example

```js
import { compileString } from 'cashc-web';

compileString('YOUR_CASHSCRIPT_CODE');
```
