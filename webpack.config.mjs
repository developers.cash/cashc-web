import path from 'path';

const outputs = [
  {
    path: path.resolve('dist'),
    filename: 'index.mjs',
    library: {
      type: 'module',
    }
  },
  {
    path: path.resolve('dist'),
    filename: 'index.cjs',
    library: {
      type: 'commonjs-static',
    }
  }
]

const targets = outputs.map((output) => {
  return {
    entry: './index.mjs',
    output,
    module: {
      rules: [
        {
          test: /\.mjs$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader', // Optional: Use Babel for transpilation
            options: {
              presets: ['@babel/preset-env'],
            },
          },
        },
      ],
    },
    experiments: {
      outputModule: true,
    },
    resolve: {
      fallback: {
        assert: 'assert/',
        fs: false,
        path: false,
        util: 'util/',
      }
    }
  }
});

export default targets;
